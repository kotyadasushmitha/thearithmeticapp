//
//  ViewController.swift
//  The ArithMETic App
//
//  Created by Student on 2/14/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource{
    
    
    @IBOutlet weak var picker: UIPickerView!
    
    
    @IBOutlet weak var weightTF: UITextField!
    @IBOutlet weak var exerciseTF: UITextField!
    @IBOutlet weak var caloriesLBL: UILabel!
    @IBOutlet weak var timePoundLBL: UILabel!
    
    
    @IBAction func clear(_ sender: Any) {
        weightTF.text! = ""
        exerciseTF.text! = ""
        caloriesLBL.text! = "0 cal"
        timePoundLBL.text! = "0 minutes"
        picker.selectRow(0, inComponent: 0, animated: true)
        
    }
    
    
    
    var pickerData: [String] = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.view.backgroundColor = #colorLiteral(red: 0.8659811616, green: 0.681150496, blue: 0.8648683429, alpha: 1)
        
        self.picker.delegate = self
        self.picker.dataSource = self
        pickerData = ["Bicycling", "Jumping rope", "Running - slow", "Running - fast", "Tennis", "Swimming"]
    }
    
    func energyConsumed(during: String, weight:Double, time:Double) -> Double {
        var activityMet = 0.0
        switch during{
        case "Bicycling","Tennis" : activityMet = 8.0
        case "Jumping rope" : activityMet = 12.3
        case "Running - slow" : activityMet = 9.8
        case "Running - fast" : activityMet = 23.0
        case "Swimming" : activityMet = 5.8
        default : activityMet = 0.0
            
        }
        let weightKG = Double(weight)/2.2
        return activityMet * 3.5 * weightKG/Double(200) * time
        
    }
    func timeToLose1Pound(during: String, weight: Double) -> Double {
        let energyConsumedInCalories = energyConsumed(during: during, weight: weight, time: 1.0)
        if weight == 0.0 {
            return 0
        }
        else{
        return Double(3500)/energyConsumedInCalories
        }
    }
    
    @IBAction func calculate(_ sender: Any) {
        let value = pickerData[picker.selectedRow(inComponent: 0)]
        let defaultValue = 0.0
        let energy = energyConsumed(during: value, weight: Double(weightTF.text!) ?? defaultValue, time: Double(exerciseTF.text!) ?? defaultValue)
        caloriesLBL.text = String(format: "%.1f cal", energy)
        let timetoloosepound = timeToLose1Pound(during: value, weight: Double(weightTF.text!) ?? defaultValue)
        timePoundLBL.text = String(format: "%.1f minutes", timetoloosepound)
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
}

